#!/usr/bin/env bash

kubectl create ns test
export KUBECONFIG=$HOME/.kube/food-config

helm upgrade --install food-api ./chart -f chart/values-test.yaml -n test
