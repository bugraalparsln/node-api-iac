#!/usr/bin/env bash

kubectl create ns prod
export KUBECONFIG=$HOME/.kube/food-config

helm upgrade --install food-api ./chart -f chart/values-prod.yaml -n prod
