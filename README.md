# Cluster generation
To test the demo locally, local kubernetes cluster `kind` is used.
Follow [this](https://kind.sigs.k8s.io/docs/user/quick-start/#installation)
link to install the kind CLI.

Then create a local cluster and name it `food`:  
`kind create cluster --name food`

Get kubeconfig and save it to a file:  
`kind get kubeconfig --name food > $HOME/.kube/food-config`

Switch your kubernetes context to newly created cluster:  
`export KUBECONFIG=$HOME/.kube/food-config`

Make sure everything is up and running:
```sh
$ kubectl get nodes
NAME                 STATUS   ROLES           AGE   VERSION
food-control-plane   Ready    control-plane   3d    v1.27.3
```

# Install ArgoCD
Checkout this repository and run `tools/argocd/install-argocd.sh`. This creates
ArgoCD in the `argocd` namespace of your cluster.

# Install food-api
For this repo, a Node API application (the one that is provided with slight changes)
is used. Source code for the app is available at [here](https://gitlab.com/bugraalparsln/node-api/-/tree/main).  
To install food-api in the cluster, navigate to `services/food-api/chart` and run
`deploy-prod.sh` and `deploy-test.sh`. These scripts install two versions of 
food-api with Helm, one test version and one production version, in separate namespaces.

# Install ArgoCD application files for food-api
Navigate to `tools/argocd` and run `intsall.sh`. This installs ArgoCD application
configs in the cluster. In order for these application configs to track the food-api
deployment, `argocd repo add` command should be executed by someone who has a 
write access to this repository.

# Go to ArgoCD UI
Port forward ArgoCD server to your localhost to access the UI:  
`kubectl port-forward -n argocd svc/argocd-server 8080:80`

Navigate to `localhost:8080` in your browser. ArgoCD asks for a login. The username
is `admin` and password can be retrieved by the following:  
`kubectl get secret -n argocd argocd-initial-admin-secret -o jsonpath={.data.password} | base64 -d`

If everything is done correctly, this should be the image:  

![ArgoCD dashboard with two applications](img/argo-db.png "")
