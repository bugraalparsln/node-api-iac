#!/usr/bin/env bash


export KUBECONFIG=$HOME/.kube/food-config

kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
