#!/usr/bin/env bash

set -e

export KUBECONFIG=$HOME/.kube/food-config

helm upgrade --install argocd-apps ./chart -n argocd
